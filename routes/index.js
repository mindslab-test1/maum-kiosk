const express = require('express');
const router = express.Router();

const validate = require('../public/utils/validation');

router.get('/', (req, res) => {
    if(!req.session.user){
        res.redirect('/login')
    }
    else{
        res.redirect('/insight');     
    }
});

router.get('/login', (req, res)=> {
    if(req.session.user){
        console.log('You already logged in');
        res.redirect('/insight');
    }
    else {
        res.render('login');
    }
});

router.get('/install', (req, res) => {
    if(req.session.user){
        res.render('install');
    } 
    else {
        res.redirect('/');
    }
});

router.post('/login', (req, res) => {
    let id = req.body.id;
    let pw = req.body.pw;
    console.log('login')
    console.log(req.body)
    if(req.session.user){
        console.log('You already logged in');
        res.redirect('/insight');
    }
    else {
        if(validate.admin(id, pw)){
            req.session.user = {
                id: id,
                role: 'admin'
            }
            req.session.save();
        }
    }
    res.redirect('/')
});

router.post('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/');
});

module.exports = router;