const express = require("express");
const router = express.Router();
const logger = require("../config/logger")(__filename);
const multer = require("multer");
const upload = multer();

// DB
const User = require("../models/User");
const TimeTable = require("../models/TimeTable");
const moment = require("moment");
const mongoose = require("mongoose");

router.get("/", (req, res) => {
  if (req.session.user) {
    res.render("secret");
  } else {
    res.redirect("/");
  }
});

router.post("/delete", async (req, res) => {
  const { data } = req.body;
  console.log(data);
  try {
    await TimeTable.deleteMany({ _id: data });
    res.status(200).send("done");
  } catch (err) {
    logger.error(err);
    res.status(500).send("Can not delete");
  }
});

router.post("/user", async (req, res) => {
  const { data } = req.body;
  try {
    const results = await User.find({ name: data.name });
    console.log(results);
    res.status(200).send({ payload: results });
  } catch (err) {
    logger.error(err);
  }
});

router.post("/date:add", async (req, res) => {
  try {
    const { userId, date, clockIn, clockOut } = req.body;
    const userObjectId = mongoose.Types.ObjectId(userId);
    const dayStart = moment(date).startOf("day");
    const clockInTime = moment(`${date} ${clockIn}`);
    const clockOutTime = moment(`${date} ${clockOut}`);

    const timeTable = new TimeTable({
      user: userObjectId,
      date: dayStart,
      clockIn: clockInTime,
      clockOut: clockOutTime,
    });
    await timeTable.save();
    res.status(200).send(timeTable);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.post("/");
module.exports = router;
