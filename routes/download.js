const express = require('express');
const router = express.Router();
const logger = require('../config/logger')(__filename);
const moment = require('moment');
const {Parser} = require('json2csv');
const x1 = require('excel4node');

// DB
const User = require('../models/User');
const TimeTable = require('../models/TimeTable');

router.post('/xlsx', async (req, res) => {
    let name = req.body.name;
    let startDate = req.body.startDate;
    let endDate = req.body.endDate;
    
    let wb1 = new x1.Workbook();
    const titleStyle = wb1.createStyle({
        alignment: {
            horizontal:['center'],
            vertical: ['center']
        },
        font:{
            size:12,
            bold: true
        },
        border: {
            top: {
                style: 'thin',
                colort: '#000000'
            },
            bottom: {
                style: 'thin',
                colort: '#000000'
            },
            right: {
                style: 'thin',
                colort: '#000000'
            },
            left: {
                style: 'thin',
                colort: '#000000'
            },
        },
        fill: {
            type: 'pattern',
            patternType: 'solid',
            bgColor: '#FFFF00',
            fgColor: '#FFFF00',
        }
    });
    const dateStyle = wb1.createStyle({
        alignment: {
            horizontal:['center'],
            vertical: ['center']
        },
        numberFormat: 'yyyy-mm-dd'
    });
    const nameStyle = wb1.createStyle({
        alignment: {
            horizontal:['center'],
            vertical: ['center']
        }
    });

    let ws = wb1.addWorksheet('Sheet');
    ws.cell(1,1).string('날짜').style(titleStyle);
    ws.cell(1,2).string('이름').style(titleStyle);
    ws.cell(1,3).string('출근').style(titleStyle);
    ws.cell(1,4).string('퇴근').style(titleStyle);
    
    let excelJson = await createExcelJson(name,startDate, endDate);

    for(let i = 0; i < excelJson.length; i ++){
        let row = i + 2;
        
        ws.cell(row,1).date(excelJson[i].date).style(dateStyle);
        ws.cell(row,2).string(excelJson[i].name).style(nameStyle);
        ws.cell(row,3).string(excelJson[i].clockIn);
        ws.cell(row,4).string(excelJson[i].clockOut);
    }
    
    ws.column(1).setWidth(15);
    ws.column(3).setWidth(13);
    ws.column(4).setWidth(13);
    wb1.write('log.xlsx',res);
});

const createExcelJson = async (name,start, end) => {
    let records = [];
    let startDate = moment(start).startOf('day');
    let endDate = moment(end).endOf('day');
    let timetables = null;
    if(name){
        let user = await User.findOne({name});
        if(!user){ return res.status(400).json({msg:'일치하는 이름이 없습니다.'});}
        timetables = await TimeTable.find({user: user.id,date:{$gte: startDate, $lte: endDate}}).populate('user', 'name').sort({date: 1});
    }else{
        timetables = await TimeTable.find({date:{$gte: startDate, $lte: endDate}}).populate('user', 'name').sort({date: 1});
    }
    
    timetables.forEach( timetable => {
        let row = {};
        row.date = moment(timetable.date).format('YYYY-MM-DD');
        row.name = timetable.user.name;
        row.clockIn = moment(timetable.clockIn).format('HH:mm:ss');
        let clockout;
        if(timetable.clockOut == null){clockout = null;}
        else {clockout = timetable.clockOut;}
        row.clockOut = moment(clockout).format('HH:mm:ss');
        records.push(row);
    });
    return records;
}

// (err)=>{
//     if(err){ return res.status(500).json({msg: "Database Error"}); }
// }
module.exports = router;