const express = require("express");
const router = express.Router();
const logger = require("../config/logger")(__filename);
const multer = require("multer");
const upload = multer();
const FormData = require("form-data");
const querystring = require("querystring");
const axios = require("axios");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");

const fs = require("fs");
const fsExtra = require("fs-extra");

const insightStub = require("../public/stubs/insight/insightStub")(
  "182.162.19.10:56101"
);
const faceRecogAppServiceStub = require("../public/stubs/insight/recogServiceStub")(
  "182.162.19.10:56200"
);

const validate = require("../public/utils/validation");

const { WebClient } = require("@slack/web-api");
const { IncomingWebhook } = require("@slack/webhook");
// To Do => Have to change it dynamically, maybe?
const token = process.env.SLACK_AUTH_TOKEN;
const botToken = process.env.SLACK_BOT_TOKEN;
const channel = process.env.SLACK_CHANNEL;
const url = process.env.SLACK_WEBHOOK_URL;
const web = new WebClient(botToken);

// DB
const User = require("../models/User");
const TimeTable = require("../models/TimeTable");

router.get("/", (req, res) => {
  if (req.session.user) {
    res.render("insight");
  } else {
    res.redirect("/");
  }
});

router.post("/setFace", upload.single("file"), (req, res) => {
  let apiId = req.body.apiId;
  let apiKey = req.body.apiKey;
  let faceId = req.body.faceId;
  let file = req.file;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;
  if (!apiId || !apiKey || !faceId || !file) {
    logger.error("setFace => Missing Parameter");
    res.sendStatus(400);
  } else {
    if (!validate.user(res, apiId, apiKey)) {
      logger.debug("apiId or apiKEY is not matched");
      res.send({ msg: "apiId or apiKEY is not matched" });
      return;
    }
  }

  let dbId = req.body.dbId;
  if (dbId == undefined) dbId = "default";

  insightStub.getFaceVector(
    { name: "", faceImg: file.buffer },
    (err, result) => {
      if (err) logger.error(err);
      faceRecogAppServiceStub.setFace(
        {
          apiId: apiId,
          dbId: dbId,
          faceId: faceId,
          faceVector: result.faceVector,
        },
        (err, result) => {
          if (err) logger.error(err);

          let output = {};
          output.message = {
            message: result.message,
            status: result.status,
          };
          output.data = {
            result: true,
            name: faceId,
          };

          logger.info(
            `setFace[${ip}]\n${apiId} => ${JSON.stringify(output.data)}`
          );
          res.send(output);
        }
      );
    }
  );
});

router.post("/recogFace", upload.single("file"), (req, res) => {
  let apiId = req.body.apiId;
  let apiKey = req.body.apiKey;
  let file = req.file;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;
  const uuid = uuidv4();
  logger.info(`/insight/recogFace [${ip}] req\n${uuid}`);

  if (!apiId || !apiKey || !file) {
    logger.error("recogFace => Missing Parameter");
    res.sendStatus(400);
  } else {
    if (!validate.user(res, apiId, apiKey)) {
      logger.debug("apiId or apiKEY is not matched");
      res.send({ msg: "apiId or apiKEY is not matched" });
      return;
    }
  }

  let dbId = req.body.dbId;
  if (dbId == undefined) dbId = "default";
  insightStub.getFaceVector(
    { name: "", faceImg: file.buffer },
    (err, result) => {
      if (err) logger.error(err);
      faceRecogAppServiceStub.recogFace(
        { apiId: apiId, dbId: dbId, faceVector: result.faceVector },
        async (err, result) => {
          if (err) logger.error(err);
          let userName = result.result.id;

          let output = {};
          output.message = {
            message: result.message,
            status: result.status,
          };
          if (userName != "__no__match__") {
            deleteImageLogDir();

            let imageLogPath = "";
            if (!fs.existsSync(process.env.ImageLog)) {
              fs.mkdirSync(process.env.ImageLog);
            }
            imageLogPath =
              process.env.ImageLog + "/" + moment().format("YYYYMMDD");
            if (!fs.existsSync(imageLogPath)) {
              fs.mkdirSync(imageLogPath);
            }

            // MongoDB
            // 사람찾고 없으면 추가
            let user = await User.findOne({ name: userName });
            if (!user) {
              user = new User({
                name: userName,
              });
              await user.save((err, data) => {
                if (err) logger.error(err);
                user = data;
              });
            }
            let currentHour = parseInt(moment().format("HH"));
            let dayStart;
            let dayEnd;
            if (currentHour < 5) {
              dayStart = moment().subtract(1, "days").startOf("day");
              dayEnd = moment().subtract(1, "days").endOf("day");
            } else {
              dayStart = moment().startOf("day");
              dayEnd = moment().endOf("day");
            }

            // 찾은 사람의 오늘 타임 테이블 찾고 없으면 추가
            let timeTable = await TimeTable.findOne({
              user: user.id,
              date: {
                $gte: dayStart,
                $lte: dayEnd,
              },
            });
            if (!timeTable) {
              timeTable = new TimeTable({
                user: user.id,
                date: dayStart,
                clockIn: moment(),
              });
              await timeTable.save();
            } else {
              await TimeTable.findByIdAndUpdate(
                timeTable.id,
                { $set: { clockOut: moment() } },
                (err) => {
                  if (err) {
                    console.log(err);
                  }
                }
              );
            }

            let currentTime = moment().format("HHmmss");
            fs.writeFile(
              imageLogPath + "/" + userName + "-" + currentTime,
              file.buffer,
              (err) => {
                if (err) {
                  logger.error(
                    `recogFace[${ip}]\n${apiId} => Saving Image, ${err}`
                  );
                }
              }
            );

            output.data = {
              result: true,
              name: userName,
            };
          } else {
            output.data = {
              result: false,
            };
          }

          logger.info(
            `/insight/recogFace [${ip}] res\n${apiId} => ${JSON.stringify(
              output.data
            )} ${uuid}`
          );
          res.send(output);
        }
      );
    }
  );
});

function deleteImageLogDir() {
  fs.readdir(process.env.ImageLog, (err, dirs) => {
    dirs.forEach((dir) => {
      let currentDir = process.env.ImageLog + "/" + dir;
      const { birthtime } = fs.statSync(currentDir);
      let created = moment(birthtime);
      let now = moment();
      let duration = moment.duration(now.diff(created)).asDays();
      if (duration > process.env.ImageLogDuration) {
        fsExtra.remove(currentDir, (err) => {
          if (err) {
            logger.error(`DeleteImageLog => ${err}`);
          }
        });
      }
    });
  });
}

router.post("/getFaceList", upload.fields([]), (req, res) => {
  let apiId = req.body.apiId;
  let apiKey = req.body.apiKey;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;

  if (!apiId || !apiKey) {
    console.log(req.body);
    logger.error("getFaceList => Missing Parameter");
    res.sendStatus(400);
  } else {
    if (!validate.user(res, apiId, apiKey)) {
      logger.debug("apiId or apiKEY is not matched");
      res.send({ msg: "apiId or apiKEY is not matched" });
      return;
    }
  }
  let dbId = req.body.dbId;
  if (dbId == undefined) dbId = "default";

  faceRecogAppServiceStub.getFaceList(
    { apiId: apiId, dbId: dbId },
    (err, result) => {
      if (err) logger.error(err);

      let names = [];
      result.payload.forEach((element) => {
        names.push(element.id);
      });
      names.sort();

      logger.info(`getFaceList[${ip}]\n${apiId} => ${names}`);
      res.send(names);
    }
  );
});

router.post("/callUser", upload.fields([]), (req, res) => {
  let apiId = req.body.apiId;
  let apiKey = req.body.apiKey;
  let name = req.body.name;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;

  if (!apiId || !apiKey || !name) {
    console.log(req.body);
    logger.error("callUser => Missing Parameter");
    res.sendStatus(400);
  } else {
    if (!validate.user(res, apiId, apiKey)) {
      logger.debug("apiId or apiKEY is not matched");
      res.send({ msg: "apiId or apiKEY is not matched" });
      return;
    }
  }
  let dbId = req.body.dbId;
  if (dbId == undefined) dbId = "default";

  faceRecogAppServiceStub.getFaceList(
    { apiId: apiId, dbId: dbId },
    async (err, result) => {
      if (err) logger.error(err);

      let names = [];
      result.payload.forEach((element) => {
        names.push(element.id);
      });
      names.sort();
      let exists = names.includes(name);

      let output = {};
      output.data = {
        result: false,
        name: name,
      };

      if (exists) {
        axios
          .post(
            `https://slack.com/api/users.list`,
            querystring.stringify({
              token: botToken,
              // channel: channel,
              limit: 9999,
            }),
            {
              headers: { "Content-Type": "application/x-www-form-urlencoded" },
            }
          )
          .then((resMembers) => {
            resMembers.data.members.forEach((member, index, arr) => {
              let realName = member.profile.real_name;
              let displayName = member.profile.display_name;

              // ex) 홍 길동 => 홍길동 || 길동 홍 => 홍길동
              if (realName.match(/\s/g)) {
                let s = realName;
                let splitedName = realName.split(/\s/g);
                if (splitedName[1].length == 1) {
                  realName = splitedName[1];
                  realName += splitedName[0];
                } else {
                  realName = splitedName[0];
                  realName += splitedName[1];
                }
                realName.replace(/\s/g, "");
              }

              // ex) 홍 길동 => 홍길동 || 길동 홍 => 홍길동
              if (displayName.match(/\s/g)) {
                let splitedName = displayName.split(/\s/g);
                if (splitedName[1].length == 1) {
                  displayName = splitedName[1];
                  displayName += splitedName[0];
                } else {
                  displayName = splitedName[0];
                  displayName += splitedName[1];
                }
              }
              if (realName == name || displayName == name) {
                // console.log("보냄 => " + name + ", " + member.id)
                const webhook = new IncomingWebhook(url);
                webhook.send({
                  text: `<@${member.id}>님 방문객이 도착하였습니다.`,
                });

                output.data.result = true;
                res.send(output);
                return;
              } else if (arr.length - 1 == index) {
                try {
                  res.send(output);
                } catch (error) {
                  logger.error(error);
                }
              }
            });
          });
      } else {
        res.send(output);
      }
    }
  );
});

function getData(name, ip, apiId) {
  return new Promise((resolve, reject) => {
    let inChannel = false;
    let users = {};
    // first axios get members Id in Slack channel
    // second axios get member's real name by Id
    axios
      .post(
        `https://slack.com/api/conversations.members`,
        querystring.stringify({
          token: botToken,
          channel: channel,
          limit: 9999,
        }),
        {
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }
      )
      .then((resMembers) => {
        console.log(resMembers);
        let members = resMembers.data.members;
        let count = 0;
        for (let i = 0; i < members.length; i++) {
          getRealName(members[i]).then((response) => {
            count++;
            users[response] = members[i];

            if (members.length == count) {
              resolve(users);
            }
          });
        }
      });
  });
}

function getRealName(member) {
  return new Promise((resolve, reject) => {
    try {
      axios
        .post(
          `https://slack.com/api/users.info`,
          querystring.stringify({
            token: botToken,
            user: member,
          }),
          {
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
          }
        )
        .then((resUserInfo) => {
          let realName = resUserInfo.data.user.real_name;
          resolve(realName);
        });
    } catch (err) {
      console.log(err);
    }
  });
}

function getResult(data, name) {
  return new Promise((resolve, reject) => {
    // ex => 철수 김
    let nameForm1 = "";
    if (nameForm1.match(/\s/g)) {
      let splitedName = nameForm1.split(/\s/g);
      nameForm1 += splitedName[1];
      nameForm1 += splitedName[0];
    }

    if (data[name]) {
      console.log("보냄 => " + data[name]);
      resolve(true);
    } else if (data[nameForm1]) {
      console.log("보냄 => " + data[nameForm1]);
      resolve(true);
    } else {
      resolve(false);
    }
  });
}

router.post("/deleteFace", upload.fields([]), (req, res) => {
  let apiId = req.body.apiId;
  let apiKey = req.body.apiKey;
  let faceId = req.body.faceId;
  let ip = req.header["x-forwarded-for"] || req.socket.remoteAddress;
  if (!apiId || !apiKey) {
    logger.error("deleteFace => Missing Parameter");
    res.sendStatus(400);
  }
  let dbId = req.body.dbId;
  if (dbId == undefined) dbId = "default";

  faceRecogAppServiceStub.deleteFace(
    { apiId: apiId, dbId: dbId, faceId: faceId },
    (err, result) => {
      if (err) logger.error(err);

      logger.info(`deleteFace[${ip}]\n${apiId} => ${JSON.stringify(result)}`);
      res.send(result);
    }
  );
});

router.post("/timeTable", upload.fields([]), async (req, res) => {
  let apiId = req.body.apiId;
  let apiKey = req.body.apiKey;
  let name = req.body.name;
  let startDate = req.body.startDate;
  let endDate = req.body.endDate;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;
  if (!apiId || !apiKey) {
    logger.error("timeTable => Missing Parameter");
    return res.status(400).json({ msg: "Invalid ID & PW" });
  }

  let data = null;
  if (name) {
    let user = await User.findOne({ name });
    if (!user) {
      return res.status(400).json({ msg: "일치하는 이름이 없습니다." });
    }
    data = await TimeTable.find({
      user: user.id,
      date: {
        $gte: moment(startDate).startOf("day"),
        $lte: moment(endDate).startOf("day"),
      },
    })
      .populate("user", "name")
      .sort("date");
  } else {
    data = await TimeTable.find({
      date: {
        $gte: moment(startDate).startOf("day"),
        $lte: moment(endDate).startOf("day"),
      },
    })
      .populate("user", "name")
      .sort("date");
  }
  if (!data) {
    return res.status(400).json({ msg: "데이터가 없습니다" });
  }
  res.status(200).json({ payload: data });
});

router.post("/updateTimeTable", upload.fields([]), (req, res) => {
  const queries = JSON.parse(req.body.queries);
  queries.forEach((query) => {
    query.types.forEach(async (type) => {
      if (type.set == "name") {
        let queryForm = { $set: { name: type.to } };
        const timeTable = await TimeTable.findById(query.objectId);
        await User.findByIdAndUpdate(timeTable.user, queryForm, (err) => {
          if (err) {
            return res.status(400).json({ msg: "Invalid Data" });
          }
        });
      } else if (type.set == "date") {
        let queryForm = { $set: { date: moment(type.to) } };
        await TimeTable.findByIdAndUpdate(query.objectId, queryForm, (err) => {
          if (err) {
            return res.status(400).json({ msg: "Invalid Data" });
          }
        });
      } else if (type.set == "clockIn" || type.set == "clockOut") {
        const timeTable = await TimeTable.findById(query.objectId);
        const date = moment(timeTable.date).format("YYYY-MM-DD");
        const time = type.to;
        const modifyTime = moment(date + " " + time);

        let queryForm = "";
        if (type.set == "clockIn") {
          queryForm = { $set: { clockIn: modifyTime } };
        } else if (type.set == "clockOut") {
          queryForm = { $set: { clockOut: modifyTime } };
        }
        await TimeTable.findByIdAndUpdate(query.objectId, queryForm, (err) => {
          if (err) {
            return res.status(400).json({ msg: "Invalid Data" });
          }
        });
      }
    });
  });
  res.status(200).json({ msg: "update success" });
});

module.exports = router;
