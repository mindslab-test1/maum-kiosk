const express = require("express");
const router = express.Router();
const logger = require("../config/logger")(__filename);
const multer = require("multer");
const upload = multer();
const async = require("async");

const lang = require("../public/stubs/common/lang");
const ttsStub = require("../public/stubs/tts/ttsStub")("114.108.173.125:30862");
const cnnSttStub = require("../public/stubs/stt/cnnSttStub")(
  "114.108.173.112:15001"
);

const validate = require("../public/utils/validation");
const { v4: uuidv4 } = require("uuid");

router.post("/stt", upload.single("file"), (req, res) => {
  let apiId = req.body.ID;
  let apiKey = req.body.key;
  let file = req.file;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;
  const uuid = uuidv4();
  logger.info(`/api/stt [${ip}]\n${uuid}`);

  if (!apiId || !apiKey || !file) {
    logger.error("/api/stt => Missing Parameter");
    res.sendStatus(400);
  } else {
    if (!validate.user(res, apiId, apiKey)) {
      logger.debug("apiId or apiKEY is not matched");
      res.send({ msg: "apiId or apiKEY is not matched" });
      return;
    }
  }

  let call = cnnSttStub.recognize((err, result) => {
    if (err) logger.error(err);

    let output = {};
    output.data = result.txt;
    output.extra_data = {
      stt_data: null,
      stt_duration: null,
      stt_length: null,
    };
    output.status = "Success";

    logger.info(
      `/api/stt [${ip}]\n${apiId} => ${JSON.stringify(output.data)} ${uuid}`
    );
    res.send(output);
  });

  async.series(call.write({ bin: file.buffer }), () => {
    call.end();
  });
});

router.post("/voice4", upload.fields([]), (req, res) => {
  let apiId = req.body.ID;
  let apiKey = req.body.key;
  let text = req.body.text;
  let ip = req.header("x-forwarded-for") || req.socket.remoteAddress;
  const uuid = uuidv4();
  logger.info(`/api/voice4 [${ip}] req\n${uuid}`);

  if (!apiId || !apiKey || !text) {
    logger.error(JSON.stringify(req.body));
    logger.error("/api/voice4 => Missing Parameter");
    res.sendStatus(400);
  } else {
    if (!validate.user(res, apiId, apiKey)) {
      logger.debug("apiId or apiKEY is not matched");
      res.send({ msg: "apiId or apiKEY is not matched" });
      return;
    }

    let call = ttsStub.speakWav(
      { text: text, lang: "ko_KR", speaker: "kor_female3", samplerate: 22050 },
      (err) => {
        if (err) logger.error(err);
      }
    );
    let arr = [];
    call.on("data", (result) => {
      arr.push(result.mediaData);
    });
    call.on("end", () => {
      let buf = Buffer.concat(arr);

      logger.info(`/api/voice4 [${ip}] res\n${apiId} => ${text} ${uuid}`);
      res.type("audio/x-wav");
      res.write(buf, "binary");
      res.end(null, "binary");
    });
  }
});

module.exports = router;
