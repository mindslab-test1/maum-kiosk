const express = require('express');
const logger = require('../../config/logger')(__filename);

let shouldBeDB = [
    {
        apiId: "i4vine-dev-api-id",
        apiKey: "95638dbe8632423d94dcb2ea11fbb0c8"
    },
    {
        apiId: "maum-ai-kiosk",
        apiKey: "b902d4ae6b5b482e84a1cab04548bac5"
    }
]

const validate = {
    user: (res, apiId, apiKey) => {
        let valid = false;
        shouldBeDB.forEach(client => {
            if(client.apiId == apiId){
                if(client.apiKey == apiKey){
                    valid = true;
                }
            }
        });
        return valid;
    },
    admin: (id, pw) => {
        if( id === process.env.adminId && pw ===process.env.adminPw) {
            return true;
        }
        else {
            return false;
        }
    }
}

module.exports = validate;