const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let PROTO_PATH = 'public/protos/stt/w2l.proto';
let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
);
let proto = grpc.loadPackageDefinition(packageDefinition);

let client = (destination) => {
    return new proto.maum.brain.w2l.SpeechToText(destination, grpc.credentials.createInsecure());
}

module.exports = client;