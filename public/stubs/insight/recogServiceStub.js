const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let PROTO_PATH = 'public/protos/insight/recog_service.proto';

let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
);

let proto = grpc.loadPackageDefinition(packageDefinition);
let client = (destination) => {
    return new proto.maum.brain.app.insight.FaceRecogAppService(destination, grpc.credentials.createInsecure());
}

module.exports = client;