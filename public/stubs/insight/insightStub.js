const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let PROTO_PATH = 'public/protos/insight/insight.proto';
let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
);
let proto = grpc.loadPackageDefinition(packageDefinition);

let client = (destination) => {
    return new proto.maum.brain.insight.InsightFace(destination, grpc.credentials.createInsecure());
}

module.exports = client;