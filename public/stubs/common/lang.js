const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let PROTO_PATH = 'public/protos/common/lang.proto';
let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
);
let proto = grpc.loadPackageDefinition(packageDefinition);
let client = proto.maum.common;

module.exports = client;