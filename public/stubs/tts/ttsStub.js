const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');

let PROTO_PATH = 'public/protos/tts/ng_tts.proto';
let packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
);

let proto = grpc.loadPackageDefinition(packageDefinition);

let client = (destination) => {
    return new proto.maum.brain.tts.NgTtsService(destination, grpc.credentials.createInsecure());
}

module.exports = client;