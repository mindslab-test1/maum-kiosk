require('dotenv').config();
const logger = require('../config/logger')(__filename);
const mongoose = require('mongoose');

const connectMongoDB = async () => {
    try {
        await mongoose.connect(process.env.mongoURI, {
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log("mongoDB is connected");
    } catch (err) {
        logger.error(err);
        // process.exit(1);
    }
}

module.exports = connectMongoDB;