require("dotenv").config();

const winston = require("winston");
require("winston-daily-rotate-file");
const { combine, timestamp, label, printf, colorize } = winston.format;
const path = require("path");

const logFormat = printf(({ level, label, message, timestamp }) => {
  return `${level}\t${timestamp} [${label}]\t${message}`;
});

let getLogger = (filepath) => {
  let filename = path.basename(filepath);

  let options = {
    dailyFile: {
      level: "info",
      filename: `../logs/i4vine.%DATE%.log`,
      datePattern: "YYYYMMDD",
      handleExceptions: false,
      format: combine(
        label({ label: filename }),
        timestamp({ format: "YYYY-MM-DD HH:mm:ss.SSS ZZ" }),
        logFormat
      ),
      zippedArchive: false,
      maxSize: "50m",
      maxFiles: "3d",
    },
    errorFile: {
      level: "warn",
      filename: "../logs/error.log",
      handleExceptions: true,
      format: combine(
        label({ label: filename }),
        timestamp({ format: "YYYY-MM-DD HH:mm:ss.SSS ZZ" }),
        logFormat
      ),
    },
    console: {
      level: "debug",
      handleExceptions: true,
      format: combine(
        label({ label: filename }),
        colorize(),
        timestamp({ format: "YYYY-MM-DD HH:mm:ss.SSS ZZ" }),
        logFormat
      ),
    },
  };

  let logger = winston.createLogger({
    transports: [
      new winston.transports.DailyRotateFile(options.dailyFile),
      new winston.transports.File(options.errorFile),
    ],
    exitOnError: false,
  });

  if (process.env.NODE_ENV == "production") {
    logger.add(new winston.transports.Console(options.console));
  }

  return logger;
};

module.exports = getLogger;
