const mongoose = require('mongoose');

const TimeTableSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    date: {
        type: Date,
        require: true
    },
    clockIn: {
        type: Date,
        default: null
    },
    clockOut: {
        type: Date,
        default: null
    }
});

module.exports = TimeTable = mongoose.model('timetable', TimeTableSchema);