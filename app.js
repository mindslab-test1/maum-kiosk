require("dotenv").config();

const express = require("express");
const app = express();
const session = require("express-session");
const bodyParser = require("body-parser");
const path = require("path");
const logger = require("./config/logger")(__filename);
const http = require("http");

// Connect Database
const connectMongoDB = require("./config/mongodb");
connectMongoDB();

const indexRouter = require("./routes/index");
const insightRouter = require("./routes/insight");
const apiRouter = require("./routes/api");

app.set("views", __dirname + "/public/components/views");
app.set("view engine", "ejs");
app.use(express.static("public"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true,
  })
);

app.use("/", indexRouter);
app.use("/insight", insightRouter);
app.use("/api", apiRouter);
app.use("/download", require("./routes/download"));
app.use("/secret", require("./routes/secret"));

let port = process.env.PORT || 3001;
const httpServer = http.createServer(app);
httpServer.listen(port, () => {
  console.log(`port ${port} is running`);
});
